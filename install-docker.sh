#!/bin/sh

if [ "$EUID" -ne 0 ]
then echo "Please run as root"
exit
fi

apt-get update
apt-get install -y \
apt-transport-https \
ca-certificates \
curl \
gnupg-agent \
software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

chmod +x ./install-docker-cleaner.sh
while true; do
read -p "Install docker-cleaner service? >_" yn
case $yn in
[Yy]* ) bash ./install-docker-cleaner.sh; echo "Service installed"; break;;
[Nn]* ) echo "Done without service installation"; exit;;
*) echo "Please answer yes or no.";;
esac
done

