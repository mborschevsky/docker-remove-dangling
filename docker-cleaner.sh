#!/bin/bash

remove_old_image()
{
    IMAGE=$1
    docker images | grep -w $IMAGE | tail -n +2 | awk '{print $3}' | xargs docker rmi -f
}

export -f remove_old_image

docker images | grep "dock.shakuro" | awk '{print $1}' | sort -u | xargs -n 1 -P 1 -I {} bash -c 'remove_old_image "$@"' _ {}
