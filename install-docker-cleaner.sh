#!/bin/sh

mkdir -p /usr/local/scripts/
cp -f ./docker-cleaner.sh /usr/local/scripts/
chmod +x /usr/local/scripts/docker-cleaner.sh
cp -f ./docker-cleaner.service /etc/systemd/system/
cp -f ./docker-cleaner.timer /etc/systemd/system/
systemctl enable docker-cleaner.service
systemctl enable docker-cleaner.timer
systemctl start docker-cleaner.timer
testing="systemctl list-timers --all | grep docker-clear-dangling"
echo `$testing`
