# docker-remove-dangling

Simple shell script and systemd service with timer that removes dangling (<none>) Docker containers.